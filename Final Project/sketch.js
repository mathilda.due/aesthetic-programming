let screen = 0; //Begin on introductionScreen
let act = [];
let ceo = [];
let cor = [];
let int = [];
let jan = [];
let rec = [];
let tec = [];
let uxd = [];
let id = [];
let clouds = [];
let buildings = [];
let inside = [];
let actValue = [];
let ceoValue = [];
let corValue = [];
let intValue = [];
let janValue = [];
let recValue = [];
let tecValue = [];
let uxdValue = [];
let num = 0;
let clicks = 0;
let time = 0;
let questions;
let access;
let button0, button1, button2, button3, button4, button5, button6, button7, buttonReload, buttonEnd;

function preload(){
  sky = loadImage("other_images/background.png");
  white_photos = loadImage("other_images/white_photos.png");
  error_message = loadImage("other_images/error_message.png")
  landscape = loadImage("other_images/landscape.png")
  end_message = loadImage('other_images/end_message.png')
  questions = loadJSON("questions.json");
  buildings = loadJSON("buildings.json");
  font = loadFont('excluded_font.ttf');

  for (let i = 0; i < 9; i++){
  act[i] = loadImage("new_quiz_images/act"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  ceo[i] = loadImage("new_quiz_images/ceo"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  cor[i] = loadImage("new_quiz_images/cor"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  int[i] = loadImage("new_quiz_images/int"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  jan[i] = loadImage("new_quiz_images/jan"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  rec[i] = loadImage("new_quiz_images/rec"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  tec[i] = loadImage("new_quiz_images/tec"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  uxd[i] = loadImage("new_quiz_images/uxd"+i+".png")
  }

  for (let i = 0; i < 8; i++){
  id[i] = loadImage("ID/id"+i+".png")
  }

  for (let i = 0; i < 11; i++){
  buildings[i] = loadImage("building_images/building"+i+".png")
  }

  for (let i = 0; i < 15; i++){
  inside[i] = loadImage("inside_building_images/inside"+i+".png")
  }
}

function setup (){ //Creating Canvas
  createCanvas(windowWidth, windowHeight);
  button0 = createImg("new_quiz_images/act0.png", "error");
  button1 = createImg("new_quiz_images/ceo0.png", "error");
  button2 = createImg("new_quiz_images/cor0.png", "error");
  button3 = createImg("new_quiz_images/int0.png", "error");
  button4 = createImg("new_quiz_images/jan0.png", "error");
  button5 = createImg("new_quiz_images/rec0.png", "error");
  button6 = createImg("new_quiz_images/tec0.png", "error");
  button7 = createImg("new_quiz_images/uxd0.png", "error");
  buttonEnd = createButton("I'm done");
  button0.hide();
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  button5.hide();
  button6.hide();
  button7.hide();
  buttonEnd.hide();
  }

function backdrop(){
  background(sky);
  showClouds();
}

function checkCloudsNum(){
if(clouds.length < 4){ // if there is less that the minimum amount of clouds on the screen the program adds more
  clouds.push(new Clouds()); //creating a new object
  }
}

function outClouds(){
  for (let i = 0; i < clouds.length; i++) {
    if (clouds[i].pos.x > width) {  // if a cloud is out of screen remove the cloud
      clouds.splice(i,1);
    }
  }
}

function showClouds(){
  /* This shows the clouds on the screen.This for-loop creates more clouds as long as there is less than minimum on the screen*/
  for (let i = 0; i < clouds.length; i++){
    clouds[i].show();
  }
}

function moveClouds(){
  /* This shows the clouds on the screen.This for-loop creates more clouds as long as there is less than minimum on the screen*/
  for (let i = 0; i < clouds.length; i++){
    clouds[i].move();
  }
}


function draw(){

  if (screen == 0){ //If the screen is equal to 0 show the introductionScreen
    backdrop();
    moveClouds();
    checkCloudsNum();
    outClouds();
    startScreen();
  } else if (screen == 1){ //If the screen is equal to 1 show the drawingScreen
    backdrop();
    moveClouds();
    checkCloudsNum();
    outClouds();
    infoScreen();
  } else if (screen == 2){
    backdrop()
    testScreen();
  } else if (screen == 3){
    backdrop()
    resultScreen();
  } else if (screen == 4){
    backdrop();
    mapScreen();
  } else if (screen == 5){
    backdrop();
    endMessage();
    noLoop();

  }
}

function mousePressed(){ //If the mouse is pressed when showing screen 0, switch to startDrawing
  if(screen == 0){
    screen = 1;
  } else if (screen == 1){
    screen = 2;
  } else if (screen == 3){
    screen = 4;
  }
}

function startScreen (){
  // pole
  push();
  rectMode(CORNER);
  strokeWeight(3);
  stroke(150);
  fill(160)
  rect(width/2, height/2, 30, height);
  pop();
  // pole highlight
  push();
  noStroke();
  fill(170)
  rect(width/2+5, height/2, 10, height);
  pop();
  // sign
  push();
  fill(34,139,34)
  rectMode(CENTER);
  strokeWeight(6);
  stroke(255)
  rect(width/2, height/3,600,250,20);
  fill(255);
  pop();
  // text
  push();
  textAlign(CENTER);
  textFont(font)
  fill(255);
  textSize(35);
  text('welcome  to', width/2,height/4);
  textSize(38);
  text('access  is  not  ( a )  given', width/2,height/3+15);
  textSize(30);
  text('"A  game  for  thought"', width/2,height/3+50);
  fill(196,244,250)
  textSize(25);
  text('press the mouse to start', width-250, height-50);
  pop();
}

function infoScreen(){ //Designing the drawingScreen
  push();
  textAlign(CENTER);
  rectMode(CENTER);
  textFont(font)
  fill(50);
  textSize(30);
  text('Before  you  play  the  game  we  want  to  get  to  know  you  better.', width/2,height/2-22, width-200, 200);
  text('In  the  following  segment,  choose  an  image  that  represents  you  the  most', width/2,height/2+100, width-200, 200);
  fill(196,244,250)
  textSize(25);
  text('press the mouse to move on', width-270, height-50);
  pop();
}

function testScreen(){ //Designing the drawingScreen
  if (clicks < 9){
    fill(50);
    textAlign(CENTER);
    textFont(font)
    textSize(40)
    text(questions.questions[num],width/2,80)
  }

  push();
  imageMode(CENTER);
  image(white_photos, width/2, height/2+50, 900,480);
  pop();

  button0.hide();
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  button5.hide();
  button6.hide();
  button7.hide();

  button0 = createImg("new_quiz_images/act"+num+".png", "error");
  button0.position(width/2-437, height/2-176);
  button0.style("width","185px");
  button0.mousePressed(printAct);

  button1 = createImg("new_quiz_images/ceo"+num+".png", "error");
  button1.position(width/2-207, height/2-176);
  button1.style("width","185px");
  button1.mousePressed(printCeo);

  button2 = createImg("new_quiz_images/cor"+num+".png", "error");
  button2.position(width/2-437, height/2+73);
  button2.style("width","185px");
  button2.mousePressed(printCor);

  button3 = createImg("new_quiz_images/int"+num+".png", "error");
  button3.position(width/2-207, height/2+73);
  button3.style("width","185px");
  button3.mousePressed(printInt);

  button4 = createImg("new_quiz_images/jan"+num+".png", "error");
  button4.position(width/2+23, height/2-176);
  button4.style("width","185px");
  button4.mousePressed(printJan);

  button5 = createImg("new_quiz_images/rec"+num+".png", "error");
  button5.position(width/2+252, height/2-176);
  button5.style("width","185px");
  button5.mousePressed(printRec);

  button6 = createImg("new_quiz_images/tec"+num+".png", "error");
  button6.position(width/2+23, height/2+73);
  button6.style("width","185px");
  button6.mousePressed(printTec);

  button7 = createImg("new_quiz_images/uxd"+num+".png", "error");
  button7.position(width/2+252, height/2+73);
  button7.style("width","185px");
  button7.mousePressed(printUxd);

  if (clicks > 8){
    button0.hide();
    button1.hide();
    button2.hide();
    button3.hide();
    button4.hide();
    button5.hide();
    button6.hide();
    button7.hide();
    screen = 3
    }
}

function printAct() {
  clicks++
  if (clicks < 10){
    actValue.push('act');
  }
  if (num < 8){
    num++
  }
//  console.log(actValue);
}

function printCeo() {
  clicks++
  if (clicks < 10){
    ceoValue.push('ceo');
  }
  if (num < 8){
    num++
  }
//  console.log(ceoValue);
}

function printCor() {
  clicks++
  if (clicks < 10){
    corValue.push('cor');
  }
  if (num < 8){
    num++
  }
//  console.log(corValue);
}

function printInt() {
  clicks++
  if (clicks < 10){
    intValue.push('int');
  }
  if (num < 8){
    num++
  }
//  console.log(intValue);
}

function printJan() {
  clicks++
  if (clicks < 10){
    janValue.push('jan');
  }
  if (num < 8){
    num++
  }
//  console.log(janValue);
}

function printRec() {
  clicks++
  if (clicks < 10){
    recValue.push('rec');
  }
  if (num < 8){
    num++
  }
//  console.log(recValue);
}

function printTec() {
  clicks++
  if (clicks < 10){
    tecValue.push('tec');
  }
  if (num < 8){
    num++
  }
//  console.log(tecValue);
}

function printUxd() {
  clicks++
  if (clicks < 10){
    uxdValue.push('uxd');
  }
  if (num < 8){
    num++
  }
//  console.log(uxdValue);
}

function resultScreen(){
  backdrop();
  fill(50);
  textAlign(CENTER);
  textFont(font)
  textSize(50)
  text('your  profile  is',width/2,100)
  fill(196,244,250)
  textSize(25);
  text('press the mouse to move on', width-270, height-50);

  // conditinal statement that checks which array is the longest
    if (actValue.length > ceoValue.length && actValue.length > corValue.length && actValue.length > intValue.length &&
        actValue.length > janValue.length && actValue.length > recValue.length && actValue.length > tecValue.length &&
        actValue.length > uxdValue.length){
        activist();
    } else if (ceoValue.length > actValue.length && ceoValue.length > corValue.length && ceoValue.length > intValue.length &&
        ceoValue.length > janValue.length && ceoValue.length > recValue.length && ceoValue.length > tecValue.length &&
        ceoValue.length > uxdValue.length){
        CEO();
    } else if (corValue.length > actValue.length && corValue.length > ceoValue.length && corValue.length > intValue.length &&
        corValue.length > janValue.length && corValue.length > recValue.length && corValue.length > tecValue.length &&
        corValue.length > uxdValue.length){
        corporate();
    } else if (intValue.length > actValue.length && intValue.length > ceoValue.length && intValue.length > corValue.length &&
        intValue.length > janValue.length && intValue.length > recValue.length && intValue.length > tecValue.length &&
        intValue.length > uxdValue.length){
        intern();
    } else if (janValue.length > actValue.length && janValue.length > ceoValue.length && janValue.length > corValue.length &&
        janValue.length > intValue.length && janValue.length > recValue.length && janValue.length > tecValue.length &&
        janValue.length > uxdValue.length){
        janitor();
    } else if (recValue.length > actValue.length && recValue.length > ceoValue.length && recValue.length > corValue.length &&
        recValue.length > intValue.length && recValue.length > janValue.length && recValue.length > tecValue.length &&
        recValue.length > uxdValue.length){
        receptionist();
    } else if (tecValue.length > actValue.length && tecValue.length > ceoValue.length && tecValue.length > corValue.length &&
        tecValue.length > intValue.length && tecValue.length > janValue.length && tecValue.length > recValue.length &&
        tecValue.length > uxdValue.length){
        tech_developer();
    } else if (uxdValue.length > actValue.length && uxdValue.length > ceoValue.length && uxdValue.length > corValue.length &&
        uxdValue.length > intValue.length && uxdValue.length > janValue.length && uxdValue.length > recValue.length &&
        uxdValue.length > tecValue.length){
        ux_designer();
    } else {
        error();
    }
}

function activist(){
  push();
  imageMode(CENTER);
  image(id[0], width/2, height/2, 540, 330);
  pop();
  access = 0;
}

function CEO(){
  push();
  imageMode(CENTER);
  image(id[1], width/2, height/2, 540, 330);
  pop();
  access = 1;
}

function corporate(){
  push();
  imageMode(CENTER);
  image(id[2], width/2, height/2, 540, 330);
  pop();
  access = 2;
}

function intern(){
  push();
  imageMode(CENTER);
  image(id[3], width/2, height/2, 540, 330);
  pop();
  access = 3;
}

function janitor(){
  push();
  imageMode(CENTER);
  image(id[4], width/2, height/2, 540, 330);
  pop();
  access = 4;
}

function receptionist(){
  push();
  imageMode(CENTER);
  image(id[5], width/2, height/2, 540, 330);
  pop();
  access = 5;
}

function tech_developer(){
  push();
  imageMode(CENTER);
  image(id[6], width/2, height/2, 540, 330);
  pop();
  access = 6;
}

function ux_designer(){
  push();
  imageMode(CENTER);
  image(id[7], width/2, height/2, 540, 330);
  pop();
  access = 7;
}

function mapScreen(){
  // landscape
  push();
  imageMode(LEFT, BOTTOM);
  image(landscape, 0, height-400, width,400);
  pop();
  textAlign(CENTER);
  textFont(font);
  fill(196,244,250)
  textSize(25);
  text('access  is  not  ( a )  given', width/2, 50);

  // buildings
  push();
  imageMode(CENTER);
  image(buildings[8], width/2+331, height-390, 220, 140); // mansion
  image(buildings[0], width/2+160, height-318, 140, 300); // bank
  image(buildings[9], width/2-10, height-343, 200, 350); // office
  image(buildings[5], width/2-520, height-228, 200, 120); // cyber cafe
  image(buildings[3], width/2-330, height-233, 150, 130); // bistro
  image(buildings[7], width/2-300, height-345, 100, 65); // house
  image(buildings[6], width/2-390, height-335, 80, 45); // garage
  image(buildings[1], width/2-180, height-282, 130, 230); // appartment
  image(buildings[4], width/2+345, height-218, 135, 100); // coffeshop
  image(buildings[2], width/2+250, height-203, 35, 70); // ATM
  image(buildings[10], width/2+520, height-248, 190, 160); // restaurant
  // ID card
  image(id[access], 160, 80, 240, 140);
  pop();
  appartmentTextbox();
  bankTextbox();
  bistroTextbox();
  coffeeshopTextbox();
  garageTextbox();
  houseTextbox();
  mansionTextbox();
  officeTextbox();
  restaurantTextbox();
  underground_net_cafeTextbox();

  time++
  if (time == 3400) {
    screen = 5
  }
}

function appartmentTextbox(){

  let d = dist(mouseX, mouseY, width/2-180, height-282);
  if (d < 65) {
    if (access == 2){
      push();
      imageMode(CENTER);
      image(inside[8], width/2-180, height-282, 350,200)
      pop();
    } else if (access == 3){
      push();
      imageMode(CENTER);
      image(inside[9], width/2-180, height-282, 350,200)
      pop();
    } else if (access == 7){
      push();
      imageMode(CENTER);
      image(inside[14], width/2-180, height-282, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2-180, height-282, 350,200)
      pop();
    }
      push();
      fill(50);
      rectMode(CENTER);
      textFont('Helvetica')
      textSize(13)
      text(buildings.appartment[access], width/2-180, height-195, 340, 40)
      pop();
  }
}

function bankTextbox(){
  let d = dist(mouseX, mouseY, width/2+160, height-318);
  if (d < 70) {
    push();
    imageMode(CENTER);
    image(inside[1], width/2+160, height-253, 350,200)
    pop();
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.bank[access], width/2+160, height-168, 340, 40)
  }
}


function bistroTextbox(){
  let d = dist(mouseX, mouseY, width/2-330, height-233);
  if (d < 75) {
    if (access == 4 || access == 5 || access == 6){
      push();
      imageMode(CENTER);
      image(inside[2], width/2-330, height-233, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2-330, height-233, 350,200)
      pop();
    }
      fill(50);
      rectMode(CENTER);
      textFont('Helvetica')
      textSize(13)
      text(buildings.bistro[access], width/2-330, height-148, 340, 40);
  }
}

function coffeeshopTextbox(){
  let d = dist(mouseX, mouseY, width/2+345, height-218);
  if (d < 67) {
    if (access == 0 || access == 2 || access == 3 || access == 5 || access == 7){
      push();
      imageMode(CENTER);
      image(inside[3], width/2+345, height-218, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2+345, height-218, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.coffeeshop[access], width/2+345, height-133, 340, 40);
  }
}

function garageTextbox(){
  let d = dist(mouseX, mouseY, width/2-390, height-335);
  if (d < 22) {
    if (access == 0){
      push();
      imageMode(CENTER);
      image(inside[6], width/2-390, height-335, 350,200)
      pop();
    } else if (access == 6){
      push();
      imageMode(CENTER);
      image(inside[5], width/2-390, height-335, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2-390, height-335, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.garage[access], width/2-390, height-250, 340, 40);
  }
}

function houseTextbox(){
  let d = dist(mouseX, mouseY, width/2-300, height-345);
  if (d < 32) {
    if (access == 4){
      push();
      imageMode(CENTER);
      image(inside[10], width/2-300, height-345, 350,200)
      pop();
    } else if (access == 5){
      push();
      imageMode(CENTER);
      image(inside[11], width/2-300, height-345, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2-300, height-345, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.house[access], width/2-300, height-260, 340, 40);
  }
}

function mansionTextbox(){
  let d = dist(mouseX, mouseY, width/2+331, height-390);
  if (d < 70) {
    if (access == 1){
      push();
      imageMode(CENTER);
      image(inside[7], width/2+331, height-390, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2+331, height-390, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.mansion[access],width/2+331, height-305, 340, 40);
  }
}

function officeTextbox(){
  let d = dist(mouseX, mouseY,  width/2-10, height-358);
  if (d < 100) {
    if (access == 0){
      push();
      imageMode(CENTER);
      image(inside[0], width/2-10, height-358, 350,200)
      pop();
    } else {
    push();
    imageMode(CENTER);
    image(inside[13], width/2-10, height-358, 350,200);
    pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.office[access], width/2-10, height-273, 340, 40);
  }
}

function restaurantTextbox(){
  let d = dist(mouseX, mouseY, width/2+520, height-248);
  if (d < 80) {
    if (access == 1 || access == 2 || access == 6 || access == 7){
      push();
      imageMode(CENTER);
      image(inside[4], width/2+520, height-248, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2+520, height-248, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.restaurant[access], width/2+520, height-163, 340, 40);
  }
}

function underground_net_cafeTextbox(){
  let d = dist(mouseX, mouseY, width/2-520, height-228);
  if (d < 60) {
    if (access == 0 || access == 3 || access == 6 || access == 7){
      push();
      imageMode(CENTER);
      image(inside[12], width/2-520, height-228, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2-520, height-228, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.underground_net_cafe[access], width/2-520, height-143, 340, 40);
  }
}

function error(){
  background(sky);
  for (let i = 0; i < 9; i++){
    // random images from the test
    image(act[i], random(0,width), random(0, height), 200, 200);
    image(ceo[i], random(0,width), random(0, height), 200, 200);
    image(cor[i], random(0,width), random(0, height), 200, 200);
    image(int[i], random(0,width), random(0, height), 200, 200);
    image(jan[i], random(0,width), random(0, height), 200, 200);
    image(rec[i], random(0,width), random(0, height), 200, 200);
    image(tec[i], random(0,width), random(0, height), 200, 200);
    image(uxd[i], random(0,width), random(0, height), 200, 200);
  }
  // error message
  push();
  imageMode(CENTER);
  image(error_message, width/2, height/2, 600, 350);
  pop();

  buttonReload = createButton("RELOAD"); //Button to reload a new image
  buttonReload.position(width/2+160, height/2+105);
  buttonReload.mousePressed(reload); //Refering to function reload
}

function endMessage(){
  imageMode(CENTER);
  image(end_message, width/2, height/2, 700,600);
  buttonEnd = createButton("RELOAD"); //Button to reload a new image
  buttonEnd.size(80,30);
  buttonEnd.position(width/2+170, height/2+220);
  buttonEnd.mousePressed(reload);

}

function reload() { //Function used for the button, to make the program reload, if it is pushed
  location.reload();
}
