## MiniX3

1. See RunMe [here](http://mathilda.due.gitlab.io/aesthetic-programming/MiniX3/)
2. See code [here](https://gitlab.com/mathilda.due/aesthetic-programming/-/blob/main/MiniX3/sketch.js) 

#### What have I produced?
For this miniX I did a spiral. The spiral is a curve traced at a point moving along a rotating line.

![](ThrobberSpiral.png)

#### Throbbing as a Circular repetition.

To do this miniX, it is required to understand the concept ‘throbber’.  What I understood was that; a loading icon is also known as a throbber. It's a graphical control element that's animated to show that a computer program is doing something in the background. A throbber, unlike a progress bar, does not show how much of the action has been accomplished.
-	A throbber is a visual indicator that a software is performing an action. But how do we understand its visuals?

##### Time
We all keep a careful eye on the passage of time. We look at the clock in the morning when we are sent off to work, we celebrate birthdays when we turn a year older, and we bury our loved ones when their time has come to an end. Time influences every single aspect of our lives.  But what is time exactly?  Time was born when the universe was.  We understand time as how earth circulates around itself and the sun. The circulation gives us the opportunity to measure time. Throughout history, there have been many instruments to measure time with, for example shadow and sundial, and then came the invention of the clock.  Time is always circular, and always has been.

A circular shape is unbreakable, leading to repetition. Time repeats itself.  Any circular movement reference time, and the way we measure time is by repeating.  The circular shape is the the repetition itself.

##### Loading
We know that a throbber is a visual indicator that a software is performing an action – what is commonly referred to as ‘loading.’  

The only thing a modern human is gradually waiting for is when something loads, and peoples don’t wish to wait in fact they hate waiting.  “Time is money” as they say, we move fast. This also implies that throbber has become a symbol of something that human hates, yet we would be annoyed if they were not around.

Technically, loading is the ability of processes to infinitely renew themselves.  A loader is a component of an operating system that is responsible for loading applications and libraries in computer systems. It is one of the most important phases at the beginning of a program since it stores programs in memory and prepares them for execution. We wouldn’t be aware of this process if we if we did not have a throbber to indicate it.  

Time is circular, and I think that's why a common throbber as we know it from various internet sites etc is circular. We also know that our time is repeating itself, and therefore I think throbbers are shown as any kind of movement that repeats itself.  So, the aspect of loading appears in the repetition.


##### Spiral
For this MiniX I continued working on the circular movement in a circular shape.
The spiral has the same movement patterns as a common Throbber. It circulates and it repeats the circulation.  With the spiral, I do not challenge the prevailing understanding of time because I find it difficult to redefine time.  I'd like to question whether there should be any "change" in the throbber's symbolism as we know it.


###### References
Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018)
