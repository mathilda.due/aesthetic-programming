let angle = 1000.0;
let offset = 300;
let scalar = 1.5;
let fart = 0.05;
let timer = 0;

let col = {
  r: 255,
  g: 0,
  b: 0
};

function setup() {
  createCanvas(600, 600);
  noStroke();
  background (0);
}

function draw() {
  col.r = random(0, 200);
  col.g = random(0, 250);
  col.b = random(100, 250);
  fill(col.r, col.g, col.b);
  noStroke();
textSize(32);
text('LOADING...', 230, 220);


if(frameCount % 60 == 0){
  timer++;
  }

let millisecond = millis();
if (timer<=12) {
  col.r = random(0, 200);
  col.g = random(0, 250);
  col.b = random(100, 250);
  let x = offset + cos(angle) * scalar;
  let y = offset + sin(angle) * scalar;
  fill(col.r, col.g, col.b);
  noStroke();
  ellipse(x, y, 5, 5);

  angle += fart;
  scalar += fart;

  }

else if (true) {
  col.r = random(0);
  col.g = random(0);
  col.b = random(0);
  let x = offset + cos(angle) * scalar;
  let y = offset + sin(angle) * scalar;
  fill(col.r, col.g, col.b);
  noStroke();
  ellipse(x, y, 6, 6);

  angle -= fart;
  scalar -= fart;
  if(timer > 25){
    timer = 0;
  }
}
 print(timer);

  }
