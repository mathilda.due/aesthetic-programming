###### Group 4: Maj, Mathilda, Trine, Isabella

MiniX8 - E-lit
=======

#### [RunME](https://isabellarosing.gitlab.io/aestheticprogramming/MiniX8/)  

#### [Source code](https://gitlab.com/isabellarosing/aestheticprogramming/-/blob/main/MiniX8/while_we_slept.js)

------

### Neutral War (While We Slept..) 


![Alt Text](https://gitlab.com/isabellarosing/aestheticprogramming/-/raw/main/MiniX8/MiniX8.gif)


#### Introduction to our work

Journalism in the midst of a wartime.
 
It's been claimed for a long time that Russian President Vladimir Putin has nothing to gain by going to war in Ukraine. Nonetheless, the war has become a reality. On February 24, Russia launched an invasion of Ukraine. President Vladimir Putin announced a military operation to "demilitarize and de-Nazifying Ukraine" around 06:00 Moscow time (UTC + 3).
 
Journalistic warfare has in recent years become news material and part of the Danish foreign editors' everyday practice. When the media goes to war, critical journalism becomes more difficult. In any case, we know that we are exposed to misinformation and propaganda - and as an old saying goes "In war, the first victim is the truth".
 
With information about the cruel warfare also comes a need to identify the hero and the villain, and we become enormously bad at filtering news because we get upset, bored and further on. In this way, a certain bias or mismatch arises due to preconceived opinions and perceptions. By neutralizing statements, we emphasize the real acts of war. That way, we can more easily set up the contrast between war and peace. We do this with the help of our ‘I did statements’, but also the front page aims to create awareness of how the fates of others are defined, while we confidently continue our everyday lives.
 
The work is inspired by I.E.D (Improvised Empathetic Device. The purpose of both I.E.D and our project is to give a real presence to the death and violence occurring in war.
 
 
#### Description
 
Our program is a critical artwork that addresses the war in Ukraine and as such, we wanted to make the interface poetic, yet simplistic to enhance the message that war, no matter who is fighting in it, is tragic. 
 
To inform the viewer of our intentions with the program, we created a start screen with the necessary information to understand the artwork. In order to change between screens, we used a global variable `screen_` that, tied to a conditional statement, changes the variable from 0 to 1 when the mouse is pressed. 
 
The main elements within our program are the text that stems from the three JSON files and the wav sound files that are stored together with the main sketch. We used the `preload` function to access each file, however, to load the sound we thought it better to use a `for-loop` which contains an array that we use later on to match the sound with the text. 
To control when the end quotes will be displayed, we once again made a global variable `countS` and set it to increment fter the screen changes, thereby creating a variable similar to frameCount, however it starts when we want it to. We did this to ensure that the viewer gets an equal amount of time to view the stories, no matter how long they stay at the start screen. The end quotes are controlled by two if-statements involving the countS variable, creating a timer-like creation.
 
As earlier mentioned, the text matches the sound files when they appear on the screen. To do so, we made some global variables `randomEvents, randomComment and randomI_did` that connect each JSON file with its corresponding wav file by creating a random integer between 0 to 15 (not including 15) that goes inside each text and sound array when the mouse is pressed. Since the order of the JSON- and wav files match each other, both arrays will have the same index value, thereby showing the same sentence. The `floor(random())` function is also used to create a random x- and y-position for each text, making them look like they are shaking. 
 
The `mousePressed` function gives the viewer the power to change the texts whenever, until they reach the end quotes. Also, the syntaxes that call the information from the JSON- and wav files are all displayed under the mousePressed function, so the text and sound will change with each mouse click. It doesn’t matter that all three sentences show at the same time, but the sound files need a delay, so they don’t interrupt one another. This is done with the `setTimeout` function that delays a function the amount of time one needs. However, this function does not take the mousePressed function into account, so every time the viewer presses the mouse, a new sound will start, not replacing the other but playing on top of it. The sound files are play-able until the end quotes appear, which again is controlled by a conditional statement involving the countS variable. 
 
 
#### Analysis
In the chapter [Vocable Code](https://aesthetic-programming.net/pages/7-vocable-code.html) Soon and Cox (2020) describe how the source code in itself can possess poetic qualities. Examples of choices that could make the code more poetic is the naming of variables and functions as well as the comments included in the text. As mentioned in the chapter, the specific words are not necessary for the computer to run the program and therefore are solely a means to communicate to other people reading the code. This communication has the potential to be more than informative - it can also be poetic. 
 
This leads us to the question: is our code poetic - or merely informative? In regards to the semantic layer of naming the JSON files, variables and functions we did mainly focus on making them representative of the strings, commands or functions they refer to. A part of the reason for this choice is that it made it easier to work with the code as a group when the code was as self explanatory as possible. Another reason is that we ended up focusing more on making the text in the JSON files and the final program poetic. In the light of this week’s reading there definitely is room for making the source code more poetic - especially with the theme we have chosen to address. For instance, we could have structured the code differently or given variables and so on names that play more into our theme of “hiding” the subjects in the strings. We could also have included more comments to add to the poetic layer. 
 
We sought to incorporate a poetic angle and experience by having the words read out loud. We used a few different randomly chosen voices to recite the words, to attempt a “neutral” reading of the statements. This made the work not only visual, but also let it express itself auditively. One might say that the auditory readings are accentuated, because of the very simple visual design, which leads the recipient to focus on the spoken words. This reading out loud could perhaps give a more expansive experience of the work for some recipients, while others might find it distracting. 
 
It would be interesting to further explore the effects of the readings, with the following question: By reading these “neutralized” news stories and personal statements out loud, are we simultaneously, metaphorically giving someone a voice (e.g. victims of war)? How can this be interpreted by the recipient? What are the consequences of this action and what could we have done differently?


#### Reflection



References 
------
#### Syntax:
https://p5js.org/reference/ 

https://www.youtube.com/watch?v=nGfTjA8qNDA

https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class09 (class)
 
#### News articles:
https://www.bt.dk/udland/overblik-mens-du-sov-det-er-der-sket-i-ukraine 

https://www.bt.dk/udland/overblik-mens-du-sov-det-er-der-sket-i-ukraine-0 

https://www.bt.dk/udland/mens-du-sov-det-er-der-sket-i-ukraine-i-nattens-loeb 

https://www.bt.dk/udland/overblik-mens-du-sov-det-er-sket-i-ukraine-i-nat-1 

https://www.bt.dk/udland/overblik-mens-du-sov-det-er-sket-i-ukraine-i-nat-0 

https://www.dr.dk/nyheder/udland/hvilke-loegnhistorier-fortaeller-russiske-medier 
