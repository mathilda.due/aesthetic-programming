let input, button, greeting;
let camcorder;



function setup() {
  // create canvas
  createCanvas(windowWidth,windowHeight);

  input = createInput();
  input.position(20, 65);

  button = createButton('submit');
  button.position(input.x + input.width, 65);
  button.mousePressed(greet);

  greeting = createElement('h2', 'What is your name?');
  greeting1 = createElement('h6', 'Remember to smile');
  greeting.position(20, 5);
  greeting1.position(22, 25);

  textAlign(CENTER);
  textSize(20);

//video.
camcorder = createCapture(VIDEO);
camcorder.size(windowWidth,windowHeight);
camcorder.hide();
}

function greet() {

  //makes the camera take a picture.
  camcorder.size(windowWidth,windowHeight);
  image(camcorder, 0, 0, windowWidth,windowHeight);

  //moves the button out of the canvas, when pushed.
  button.position(-1200,-1200);
  input.position(-1200, -1200);
  greeting1.position(-1200, 1200);


  const name = input.value();
  greeting.html('Hello ' + name + '!');
  input.value('');


  for (let i = 0; i < 300; i++) {
    push();
    fill(random(255), 204, 0);
    translate(random(width), random(height));
    rotate(random(4 * PI));
    text(name, 0, 0);
    pop();
  }
}
