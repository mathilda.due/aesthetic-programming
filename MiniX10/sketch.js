// Copyright (c) 2020 ml5
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

/* ===
ml5 Example
Object Detection using COCOSSD
This example uses a callback pattern to create the classifier
=== */

let objectDetector;
let img = [];
let randomImg;

function preload() {

  for (let i = 0; i < 47; i++){
   img[i] = loadImage("images/Image"+i+".png")
 }
  // Models available are 'cocossd', 'yolo'
  objectDetector = ml5.objectDetector('cocossd');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  randomImg = floor(random(0,47))
  image(img[randomImg], 0, 0);
  objectDetector.detect(img[randomImg], gotResult);

  button = createButton("RELOAD"); //Button to make a new recipe, if it is pushed
  button.position(25,70);
  button.mousePressed(reload); //Refering to function reload

}

// A function to run when we get any errors and the results
function gotResult(err, results) {
  if (err) {
    console.log(err);
  } else {
    console.log(results)
  }

  for (let j = 0; j < results.length; j += 1) {
    noStroke();
    fill(0, 255, 0);
    text(
      `${results[j].label} ${nfc(results[j].confidence * 100.0, 2)}%`,
      results[j].x + 5,
      results[j].y + 15,
    );
    noFill();
    strokeWeight(4);
    stroke(0, 255, 0);
    rect(results[j].x, results[j].y, results[j].width, results[j].height);
  }
}


function reload() { //Function used for the button, to make the program reload, if it is pushed
  location.reload();
}

/*
function mousePressed(){
  randomImg = floor(random(0,3))
  console.log(randomImg);
}*/
