## MiniX1
A mini love letter.

1. See RunMe [here](http://mathilda.due.gitlab.io/aesthetic-programming/MiniX1/)
2. See code [here](https://gitlab.com/mathilda.due/aesthetic-programming/-/blob/main/MiniX1/sketch.js) 


##### What have I produced?
Basically, I had no experience with coding, so I work from what we had shown in the introductory video. So I knew how to shape a circle. I tried to change the aspect ratio of my circle by inserting other values. When I found out how it worked, I wanted to insert other shapes, so I used P5.Js' reference list to figure out how to make a triangle. I would like to combine these forms in order to create a heart. So I had to spend time understanding how I could get better at placing the objects in the coordinate system.

I found it easier to place and understand the coordinate system by scaling it down to a smaller size. Once I had done that, I placed the objects with the same fix point so I knew where they intersected. It helped make me able to form a solid heart.

Next, I wanted to add some color to my illustation, so I made my background “pink” and my heart “red”.

After that, I was reminded that it was Valentine's Day, so I wanted to use the illustration to wish people a “Happy Valentine”. I again used P5.JS’ reference list to figure out how to add text.

All in all, in turned out to be a mini love letter.

![](HappyValentine.png)

##### How would you describe your first independent coding experience?
I think that at first it was rewarding as you directly she the changes you do in the Atom program unfold in the software you create. By following the introductory video, you already understood how to make a single liner, and I therefore got enthuastic in terms of creating other shapes and i different sizes.

The diffuculty hit when I was to use the coordinate system to position my objects very precisely.
It was not difficult to understand how colors work by having some sort of ‘color code’, but I have a hard time imagining how I, in the future, will be able to create a specfic color. Since I just used the  in P5.js' reference list so find any colour. So if I want a particular green, how can I get just this one. This is a great example of how several questions arose while I was in the process. These must be questions I hopefully will have answered in time, both by participating in the class, but also by simply trying a lot of things.
