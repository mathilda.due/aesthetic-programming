
//To make the Clown's eyes rotate i use angles.
let angle=0;
let angle2 = 0;


function setup() {
  createCanvas(700, 440);
  strokeWeight(0);
  angleMode(DEGREES);
  rectMode(CENTER)

}

function draw() {
{
    background('grey');
let pupilConstrainRightX = constrain(mouseX, 532 - 15, 532 + 15);
let pupilConstrainRightY = constrain(mouseY, 250 - 10, 250 + 10)
let pupilConstrainLeftX = constrain(mouseX, 452 - 15, 452 + 15);
let pupilConstrainLeftY = constrain(mouseY, 250 - 10, 250 + 10)


// **The Left Clown**

//The hat of the Clown
   b = color('orange');
  	fill(b);
  	ellipse(190,50, 30);
  	triangle(190, 50, 275, 220, 115, 220);

//The shape of the Clown's head
noStroke();
c = color('beige');
  fill(c);
  ellipse(90,260, 60);
  ellipse(295,260, 60);
  ellipse(193,280, 205);

// The Mouth
  e = color('red');
    fill(e);
    ellipse(192,340, 70,78);

  f = color('beige');
    fill(f);
    ellipse(192,320, 70,78);

//The nose, that simulates a void, since the random funtion changes the colors.
  g = color(200);
  var g = random(200);
  fill(g);
  ellipse(192,310, 60);

//The push and pop is to make the eyes rotate
push()
  translate(240,245)
  rotate(0.01)
  rotate(angle);

    d = color('black');
     fill(d);
    rect(0, 0, 50, 7);
    rect(0, 0, 7, 50);
pop()

push()
translate(150,245)
rotate(angle); //The rotation is determined by the angle

  d = color('black');
   fill(d);
    rect(0, 0, 50, 7);
    rect(0,0, 7, 50);
pop()

// **The Right Clown**

//The hat of the Clown
 b = color('orange');
  fill(b);
  ellipse(490,50, 30);
  triangle(490, 50, 575, 220, 415, 220);

//The shapes of the Clown's head.
noStroke();
c = color('beige');
fill(c);
ellipse(390,260, 60);
ellipse(595,260, 60);
ellipse(493,280, 205);

//Mouth
e = color('red');
 fill(e);
 ellipse(492,340, 70,78);

f = color('beige');
 fill(f);
 ellipse(492,320, 70,78);


push()
translate(452, 250);
rotate(angle2)
strokeWeight(4);
stroke(51);
noFill();
ellipse(0,0,60,70) //Left Eye
pop()

push()
translate(532, 250);
rotate(angle2)
strokeWeight(4);
stroke(51);
noFill();
ellipse(0,0,60,70) //right eye
pop()


// left iris
push()
fill(51);
ellipse(pupilConstrainLeftX, pupilConstrainLeftY, 20, 30);
pop()


// right iris
push()
fill(51);
ellipse(pupilConstrainRightX, pupilConstrainRightY ,20,30);
pop()


//The nose, that simulates a void, since the random funtion changes the colors.
g = color(200);
var g = random(200);
noStroke();
fill(g);
ellipse(492,310, 60);

//Tracks if the mouse is within a specific area.
if(mouseX >= 162 && mouseX <= 222){
//This sets the "speed" of the rotation.
angle=angle+100 // Since i defined the angle at 0, i thereby plus the angle with 100.
}


//This is the red ellipse that follows the mouse.
h = color('red');
fill(h);
ellipse(mouseX,mouseY,60);

}
}
