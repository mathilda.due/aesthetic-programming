## Sine Wave

This week’s miniX uses input and output ideas to teach the concept of the abstract machine. The generation of rules by a self-operating machine is referred to as this.  This topic lets us explore how instructions are important parts of adaptive systems, with an emphasis on how rules are implemented and how they might result in unexpected and/or complicated outcomes.  


![](SineWave.png)

#### What have I produced
For this week’s miniX, I created a Sine Wave, also known as a sinusoidal wave. A Sine Wave is a mathematical curve defined in terms of the sine trigonometric function, whose graph it is. It's a smooth periodic function and a sort of continuous wave. It can be found in many domains, including mathematics, physics, engineering, signal processing, and many more.


1. See RunMe [here](http://mathilda.due.gitlab.io/aesthetic-programming/MiniX5/)
2. See code [here](https://gitlab.com/mathilda.due/aesthetic-programming/-/blob/main/MiniX5/sketch.js) 

#### What does the work express  and what is the conceptualization
Simply, it expresses the movement of a Sine wave in a rotating 3D form. Normally a Sine Wave is seen I 2D, so by adding another dimension, you understand it differently.  
At first, I thought it was difficult to conceptualize an auto generative program. We were introduced to creative abstract patterns, which looked great, but I didn’t understand if there was a general idea of a concept. I am a pragmatic, rational thinker who appreciates when art is explained to me in a straightforward manner. As a result, I appreciate the fact that art refers to or imagines something real. This is the reason why I have chosen to depict something that refers to sound which is the concept behind my program.

The most natural representation of how many things in nature change state is a sine wave. A sine wave depicts how a variable's amplitude fluctuates over time. For example, the variable could be audible sound. Sine waves are the core components of all sounds in nature. Additional complex sounds are simply more oscillations at various frequencies stacked on top of each other. Harmonics are higher-frequency oscillations that are tonally related to the fundamental frequency. Because the mechanics of air and other materials create simultaneous tones in higher octaves, all sounds in nature produce harmonics.

#### What are the properties and behaviors of objects?
A sine wave is a periodic oscillating geometric waveform that goes up, down, or side to side. So, for the software to work, it must obey various rules to makes these movements happening. As mentioned, normally a sine wave is showed in 2D, therefore you understand the sine wave as a line, whereas the amplitude, the elevation and wavelength differ from what the sine wave depicts for example sound. We usually operate in Hertz when modeling sound waves. Sine waves with varied Hertz values produce diverse sounds, allowing us to cycle through scales in music using sine waves of various periods.  The higher the hertz, the higher the amplitude, and the longer the tone, the longer the wavelength is a simple way to grasp it.

The dimensions in our programming workspace, Atom, are referred to as 2D and 3D. 2D has only two dimensions, utilizing the horizontal and vertical (X and Y) dimensions, and when turned to the side, it becomes a line. The depth (Z) dimension is added in 3D. This third dimension enables rotation and visualization from various angles. This means you could translate the differences between 2D and 3D into a distinction between a line and a sculpture. That is what it means to have a new perspective on the sine wave.

#### Thoughts on the topic ’Auto generative program’
The topic “auto generative” or “a generative program” indicates that the code is producing something. But what does it mean to produce? We were introduced to the topic with examples of patterns, which led to an enlargement of having to create a pattern that differentiated from time to time, of course using, among other things, the Random function. I have not made use of the Random function, nor does my code led to creating a unique pattern. In fact, the code causes the same repetitive motion to occur. Yet I still feel like it is generative. It generates a pattern even though it is just a repetitive pattern.  
