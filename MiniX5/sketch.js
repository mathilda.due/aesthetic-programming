
function setup() {
createCanvas(windowWidth,windowHeight,WEBGL);
//WEBGL Enables 3D render by introducing the third dimension: Z

//Sets the current mode of p5 to given mode in this case degrees.
  // DEGREES are constant to be used with angleMode function
    // DEGREES interprets and calculates my angles.
angleMode(DEGREES)
}

function draw() {
  background(30);

// RotateX rotates my shape around the X axis by the amount (60) that i specified in the angle parameter.
  // I use this synthax to determine how we view the object.
rotateX(80)

noFill()
stroke(255)

// Sin: Calculates the sine of the angle. This function takes into account the current angleMode.
  // Cos: Calculates the cosine of the angle. This function takes into account the current angleMode
      // Map: is used to calcutes the distance

// I used the 'for loop' function executing this section of the code multiple times.
  for (let i = 0; i < 50; i++)  {
    let x = map (sin (frameCount / 2), -1, 1, 100, 200)
    let y = map (i, 0, 50, 100, 200)
    let z = map (cos (frameCount), -1, 1, 200, 100)

    stroke(x,y,z)
    rotate(frameCount/100)


// Using the beginShape and endShape function allows me to create a more complex form.
    // The beginShape begins recording the vertices i use for the shape
        // The endShape stop the recording.
beginShape()
// I used the 'for loop' function executing this section of the code multiple times.

      for (let j=0; j<360; j+=60)  {
        let rad = i * 3
        let a = rad * cos(j)
        let b = rad * sin(j)
        let c = sin(frameCount * 1 + i * 4) * 80

// As mentioned my shape is made of vertices.
  //Vertex() help me to specify the vertex coodinates my shape.
vertex(a,b,c)
}
endShape(CLOSE)
}
}
