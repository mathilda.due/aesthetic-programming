## Games with Objects

1. See RunMe [here](http://mathilda.due.gitlab.io/aesthetic-programming/MiniX6/)
2. See code [here](https://gitlab.com/mathilda.due/aesthetic-programming/-/blob/main/MiniX6/sketch.js)

#### What is the Program about?
The idea behind the program was to make a game called "avoiding game", and as the name implies it's all about avoiding the obstacles you'll come across throughout the game. It was thought of as a single player game. A single player game is a battle against some part of the environment (what I have previously mentioned as ‘obstacles’) as opposed to a game with several players competing with or against each other to attain the game's goal.

I was able to produce the 'environment', which is the circles that abound around the screen. The idea was that the character (hero) could be navigated around the surface using the arrow keys, with the emphasis on avoiding the circles, and get to the other side of the screen, whereas the circles would move in at a higher pace.  Instead, the focus of the game shifted to catching the circles. As a result, rather of acting as obstacles, it was all about earning points.

I had a lot of trouble getting this game to work, and yet it still isn’t adequate. Issues arose in checking hits and calculates these as scores, also I troubled me when I was to make sure that circles would appear constantly. None of this is still working. To be honest, it is a very inacceptable program to hand over, as it lacks numerous facets that makes you understand it as a game. I learned that I lacked the necessary skills to get that far.
The further we get into the subject, the degree of difficulty increases, and you have higher expectations of what the program should be able to accomplish, which also raises your expectations of what you can achieve.

This week has been the most difficult for me since I've had to realize that I won't be able to achieve all I wanted. Nevertheless, I was focused on understanding how we use classes, and I made that function. This is the most important part for me as it was linked to this week’s topic, Games with objects.


![](GamesWithObjects.png)

#### What I understand by OOP
Object-Oriented Programming (OOP) is a programming paradigm that organizes programs around data, or objects, rather than functions and logic. The fundamental purpose is to abstract certain details and give a concrete model to handle the complexity of an object.

The structure in Object-oriented programming includes _classes, objects, method, and attributes._  Classes are data types that are created by the user and serve as the blueprint for individual objects, properties, and actions.

Objects are instances of a class that have been constructed with specific data. Real-world items or abstract entities can be represented by objects. The description is the only object defined when a class is created for the first time. Methods are functions that describe an object's behavior and are defined within a class. In class declarations, each method begins with a reference to an instance object. In addition, instance methods refer to the subroutines that make up an object. Methods are used by programmers to ensure reusability and to keep functionality contained to a single object at a time. And attributes describe the status of an object and are defined in the class template. Data will be saved in the attributes field of objects. The class's attributes are the class's own.

OOP is a programming paradigm that organizes software design around data, rather than functions and logic. An object is a data field with its own set of properties and behavior. An object is an important idea in programming, but it can also be thought of as a thing with attributes that can be identified in connection to the term subject. Simply said, a subject is an observer (a programmer) and an object is anything outside of this, something observed, according to philosophical traditions (a program).

Thus, abstraction can be found at many different levels and sizes of computing. Naturally, the reduction in complexity is beneficial for a variety of reasons, including accessibility, but we must remember that there is more at stake. Even at the highest levels, learning to program involves us in the politics of this transition between abstract and tangible reality, which is never a neutral process.
